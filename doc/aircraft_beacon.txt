

{'address': 'DF13BF',
 'address_type': 2,
 'aircraft_type': 1,
 'altitude': 231.95280000000002,
 'aprs_type': 'position',
 'beacon_type': 'aprs_aircraft',
 'climb_rate': 2.6162,
 'comment': 'id06DDD4C8 +515fpm +0.3rot 26.5dB 0e -11.6kHz gps2x3',
 'dstcall': 'APRS',
 'error_count': 0,
 'flightlevel': None,
 'frequency_offset': -11.6,
 'gps_quality': {'horizontal': 2, 'vertical': 3},
 'ground_speed': 112.96133630930952,
 'hardware_version': None,
 'latitude': 46.92796666666667,
 'longitude': -1.3555833333333334,
 'name': 'FLRDDD4C8',
 'proximity': None,
 'raw_message': "FLRDDD4C8>APRS,qAS,LFFW:/134609h4655.67N/00121.33W'348/061/A=000761 "
                '!W85! id06DDD4C8 +515fpm +0.3rot 26.5dB 0e -11.6kHz gps2x3',
 'real_address': None,
 'receiver_name': 'LFFW',
 'reference_timestamp': datetime.datetime(2020, 7, 16, 13, 46, 15, 908213),
 'relay': None,
 'signal_power': None,
 'signal_quality': 26.5,
 'software_version': None,
 'stealth': False,
 'symbolcode': "'",
 'symboltable': '/',
 'timestamp': datetime.datetime(2020, 7, 16, 13, 46, 9),
 'track': 348,
 'turn_rate': 0.8999999999999999}